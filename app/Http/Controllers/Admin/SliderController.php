<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SliderRequest; 
use App\Models\Destination; // Tambahkan use statement
use App\Models\Slider;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class SliderController extends Controller
{
   
    public function index(): View
    {
        $sliders = Slider::get();

        return view('admin.sliders.index', compact('sliders'));
    }

    public function create(): View
    {
        $destinations = Destination::all(); // Ambil semua destinasi

        return view('admin.sliders.create', compact('destinations'));
    }

    public function store(SliderRequest $request): RedirectResponse
    {
        $validatedData = $request->validated();

        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('assets/slider', 'public');
            $validatedData['image'] = $image;
        }

        Slider::create($validatedData);

        return redirect()->route('admin.sliders.index')->with([
            'message' => 'Successfully created!',
            'alert-type' => 'success'
        ]);
    }

    public function edit(Slider $slider): View
    {
        $destinations = Destination::all(); // Ambil semua destinasi

        return view('admin.sliders.edit', compact('slider', 'destinations'));
    }

    public function update(SliderRequest $request, Slider $slider): RedirectResponse
    {
        if($request->validated()){
            if($request->image) {
                File::delete('storage/' . $slider->image);
                $image = $request->file('image')->store('assets/slider', 'public');
                $slider->update($request->except('image')+['image' => $image]);
            }else{
                $slider->update($request->validated());
            }

        }
        

        return redirect()->route('admin.sliders.index')->with([
            'message' => 'successfully updated !',
            'alert-type' => 'info'
        ]);
    }

    public function destroy(Slider $slider): RedirectResponse
    {
        File::delete('storage/' . $slider->image);
        $slider->delete();

        return back()->with([
            'message' => 'successfully deleted !',
            'alert-type' => 'danger'
        ]);
    }
}