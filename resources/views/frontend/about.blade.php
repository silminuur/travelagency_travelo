@extends('frontend.layout')

@section('content')
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_3">
      <div class="container">
        <div class="row">
          <div class="col-xl-12">
            <div class="bradcam_text text-center">
              <h3>About Us</h3>
              <p>Informasi mengenai travel agency Travelo</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/ bradcam_area  -->

    <div class="about_story">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="story_heading">
              <h3>Our Story</h3>
            </div>
            <div class="row">
              <div class="col-lg-11 offset-lg-1">
                <div class="story_info">
                  <div class="row">
                    <div class="col-lg-9">
                      <p>
                      Travelo adalah travel agency yang berkomitmen untuk memberikan pengalaman perjalanan 
                      yang tak terlupakan kepada setiap pelanggan. Dengan dedikasi pada kualitas, layanan, 
                      dan kepuasan pelanggan, Travelo telah menjadi pilihan utama bagi mereka yang mencari solusi perjalanan yang handal dan berkualitas.
                      </p>
                      <p>
                      Dengan Travelo, pelanggan bukan hanya sekadar pelancong, melainkan 
                      mitra dalam setiap perjalanan. Setiap destinasi menjadi pengalaman 
                      yang unik, dan Travelo berkomitmen untuk membuat setiap momen perjalanan menjadi berharga. 
                      Tentu saja, informasi lebih lanjut mengenai layanan dan paket perjalanan yang ditawarkan oleh Travelo dapat diperoleh dengan menghubungi tim mereka secara langsung. Travelo hadir untuk memastikan bahwa setiap perjalanan bukan hanya destinasi, melainkan cerita perjalanan yang tak terlupakan.







                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection