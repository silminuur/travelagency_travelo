<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\DestinationController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Admin\SettingController;

// Frontend Routes

Route::get('/index.html', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/about.html', function () {
    return view('frontend.about');
});
Route::get('/travel_destination.html/{destination:city}', [\App\Http\Controllers\DetailController::class, 'show'])->name('destination.show');
Route::get('/travel_destination.html', function () {
    return view('frontend.destination.index');
});
Route::get('/contact.html', function () {
    return view('frontend.contact');
});

Auth::routes();

// Admin Routes
Route::group(['middleware' => 'isAdmin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
    Route::resource('permissions', PermissionController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::get('profile', [ProfileController::class, 'show'])->name('profile.show');
    Route::put('profile', [ProfileController::class, 'update'])->name('profile.update');
    
    Route::resource('sliders', SliderController::class);
    Route::resource('destinations', DestinationController::class);
    Route::resource('testimonials', TestimonialController::class);
    Route::resource('settings', SettingController::class);
});
